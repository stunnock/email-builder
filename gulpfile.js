var
	gulp = 			require('gulp'),
	browserSync = 	require('browser-sync').create(),
	gulpsync   = 	require('gulp-sync')(gulp),
	$ = require('gulp-load-plugins')({
		pattern: ['gulp-*', 'gulp.*'],
		replaceString: /\bgulp[\-.]/,
		lazy: true,
		camelize: true
	}),
	path = 			require('path'),
	pkg = 			require('./package.json'),

	source = {
		sass : ['./assets/sass/**/*.sass'],
		img  : './assets/img/**/*.{png,jpg,svg}',
		pug  : [
			'./assets/pug/pages/**/*.pug',
			'./assets/pug/**/*.pug'
		],
		inline : './temp/html/*.html',
		php    : './assets/php/*.php'
	},
	build = {
		pug    : './build/',
		img    : './build/img',
		inline : './build/',
		php    : './build/',
		folder : './build/'
	},
	depo = {
		pug    : './assets/pug/page-depo/'
	},
	temp = {
		sass   : './temp/css/',
		pug    : './temp/html/',
		folder : './temp'
	}
;

function swallowError (error) {
	console.log(error.toString());
	this.emit('end');
};


gulp.task('sass', function () {
	return gulp.src(source.sass)
		.pipe($.sass()
			.on('error', swallowError)
		)
		.pipe(gulp.dest(temp.sass))
});

gulp.task('pug', function buildHTML() {
	return gulp.src(source.pug[0])
	.pipe($.pug({
		pretty: '	'
	}).on('error', swallowError))
	.pipe(gulp.dest(temp.pug))
});

gulp.task('inline', function() {
	return gulp.src(source.inline)
		.pipe($.inlineCss({
			applyStyleTags: true,
			applyLinkTags: true,
			removeStyleTags: true,
			removeLinkTags: false,
			preserveMediaQueries: true
		}))
		.pipe(gulp.dest(build.inline))
});


gulp.task('server', function() {
	browserSync.init({
		reloadDelay: 1000,
		open: false,
		server: {
			baseDir: "./build/",
			directory: true
		}
	});

	gulp.start('default');
	gulp.start('watch:server');
});

gulp.task('server:open', function() {
	browserSync.init({
		reloadDelay: 1000,
		server: {
			baseDir: "./build/",
			directory: true
		}
	});

	gulp.start('default');
	gulp.start('watch:server');
});


gulp.task('sftp', function () {
	return gulp.src('build/**/*')
		.pipe($.sftp({
			host: '93.174.131.50',
			user: 'bitrix',
			pass: 'WpGe9y8crNoj',
			port: '22',
			remotePath: '/home/bitrix/ext_www/go.urraa.ru/mail'
		}));
});

gulp.task('copy-img', function() {
	return gulp.src(source.img)
	.pipe($.newer(build.img))
	.pipe($.imagemin())
	.pipe(gulp.dest(build.img));
});

gulp.task('copy-php', function() {
	return gulp.src(source.php)
	.pipe(gulp.dest(build.php));
});

gulp.task('copy-pug', function() {
	return gulp.src(source.pug[0])
	.pipe(gulp.dest(depo.pug));
});

gulp.task('clean-sass', function () {
	return gulp.src(temp.sass, {read: false})
	.pipe($.clean());
});

gulp.task('clean-html', function () {
	return gulp.src(temp.pug+'/*.html', {read: false})
	.pipe($.clean());
});

gulp.task('clean-pug', function () {
	return gulp.src(source.pug[0], {read: false})
	.pipe($.clean());
});

gulp.task('clean-img', function () {
	return gulp.src(build.img, {read: false})
	.pipe($.clean());
});

gulp.task('clean-temp', function () {
	return gulp.src(temp.folder, {read: false})
	.pipe($.clean());
});

gulp.task('clean-php', function () {
	return gulp.src(build.php+'/*.php', {read: false})
	.pipe($.clean());
});

gulp.task('clean-build', function () {
	return gulp.src(build.folder, {read: false})
	.pipe($.clean());
});


/***** START IT *****/
gulp.task('img',   gulpsync.sync(['clean-img' ,'copy-img' ]));
gulp.task('copy', ['copy-img']);

gulp.task('clean', gulpsync.sync(['clean-img', 'clean-temp' ]));
gulp.task('build', gulpsync.sync(['sass','pug','inline', 'clean-temp']));

gulp.task('reset', ['clean-temp', 'clean-build' ]);
gulp.task('reset:hard', ['reset', 'clean-pug']);

gulp.task('default', ['copy', 'build']);

gulp.task('watch', function () {
	gulp.watch(source.sass, ['build']);
	gulp.watch(source.pug[1], ['build']);
	gulp.watch(source.pug[0], ['copy-pug']);
	gulp.start('default');
});

gulp.task('watch:server', function () {
	gulp.watch(source.sass,   ['build']).on('change', browserSync.reload);
	gulp.watch(source.pug[1], ['build']).on('change', browserSync.reload);
	gulp.watch(source.pug[0], ['copy-pug']);
});

gulp.task('push', gulpsync.sync(['copy-php', 'sftp', 'clean-php']))










